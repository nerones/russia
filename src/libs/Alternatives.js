
export default {
  make(matches) {
    let unfinished = matches.filter(match => !match.finished);
    let finished = matches.filter(match => match.finished);
    let alternatives = []


    unfinished.map(match => {
      let newMatches = this.alts(match);
      if (alternatives.length === 0) {
        alternatives = newMatches.map(match => [match]);
        return;
      }
      let newAlts = [];
      alternatives.map(alt => {
        newMatches.map(match => {
          let newAlt = alt.slice(0);
          newAlt.push(match);
          newAlts.push(newAlt);
        });
      });
      alternatives = newAlts;
    });

    return alternatives.map(alt => {
      return {
        all: finished.concat(alt),
        news: alt
      };
    });
  },

  alts(match) {
    let results = [
      [1, 0],
      [0, 1],
      [0, 0],
    ];
    let alts = [];
    results.map(resultados => {
      let newMatch = Object.assign({}, match);
      newMatch.home_result = resultados[0];
      newMatch.away_result = resultados[1];
      newMatch.finished = true;
      alts.push(newMatch);
    });

    return alts;
  }
}
