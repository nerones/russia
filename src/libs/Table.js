export default {
  make(groupMatches) {
    let teams = [];
    groupMatches.map(match => {
      if (!match.finished) {
        return;
      }
      this.setPoints(teams, match.home_team, match.home_result, match.away_result);
      this.setPoints(teams, match.away_team, match.away_result, match.home_result);
    });

    return teams;
  },

  setPoints(teams, teamId, goalsf, goalsc) {

    let points = 1;
    if (goalsf > goalsc) {
      points = 3;
    } else if (goalsf < goalsc ){
      points = 0;
    }

    let team = this.getTeam(teams, teamId);

    team.played += 1;
    team.points += points;
    team.goalsf += goalsf;
    team.goalsc += goalsc;
  },

  getTeam(teams, id) {
    let currentTeam = teams.find(item => item.id == id);
    if (typeof currentTeam === 'undefined') {
      currentTeam = {
        id: id,
        played: 0,
        points: 0,
        goalsf: 0,
        goalsc: 0
      };
      teams.push(currentTeam);
    }
    return currentTeam;
  },

  makeOrdered(groupMatches) {
    let table = this.make(groupMatches);
    return table.sort((a, b) => {
      return b.points - a.points;
    });
  }
};
